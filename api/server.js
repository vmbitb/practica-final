const express = require("express"); // es el nostre servidor web
const cors = require("cors"); // ens habilita el cors recordes el bicing???
const bodyParser = require("body-parser"); // per a poder rebre jsons en el body de la resposta

const app = express();
const baseUrl = "/api";
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false })); //per a poder rebre json en el reuest
app.use(bodyParser.json());

//La configuració de la meva bbdd
ddbbConfig = {
   user: 'victor-masip-7e4',
   host: 'postgresql-victor-masip-7e4.alwaysdata.net',
   database: 'victor-masip-7e4_motos',
   password: 'victorm1234',
   port: 5432
};
const Pool = require('pg').Pool
const pool = new Pool(ddbbConfig);

const getMotos = (request, response) => {
   var consulta = "SELECT * FROM  motos"
   pool.query(consulta, (error, results) => {
       if (error) {
           throw error
       }
       response.status(200).json(results.rows)
       console.log(results.rows);
   });
}
app.get(baseUrl + '/motos', getMotos);

const getMotosByMarca = (req, res) => {
    const marca = req.params.marca;
    pool.query('SELECT * FROM motos WHERE marca = $1', [marca], (error,  results) => {
      if (error) {
      throw error;
     }
     res.status(200).json(
      results.rows
     );
     });
    };
app.route(baseUrl + '/motos/:marca').get(getMotosByMarca)

const getMotosById = (req, res) => {
    const id = req.params.id;
    pool.query('DELETE FROM motos WHERE id = $1', [id], (error,  results) => {
        if (error) { throw error; }
        res.status(200).json(results.rows);
    })
}
app.route(baseUrl + '/motos/id/:id').delete(getMotosById)

const createMoto = (request, response) => {
    const body = request.body
    
    const newMoto = {
        ...body
    }    
    pool.query('INSERT INTO motos (foto, marca, modelo, year, precio) VALUES ($1, $2, $3, $4, $5)', [newMoto.marca, newMoto.modelo, newMoto.year, newMoto.precio, newMoto.foto], (error, results) => {
        if (error) {
        throw error
      }
      response.status(201).send('Inserted')
      response.send('Hola aixo es el que m\'arriva' + JSON.stringify(request.body))
    })
}
app.post(baseUrl + "/moto", createMoto);  

const PORT = process.env.PORT || 3000;
const IP = process.env.IP || null; 

app.listen(PORT, IP, () => {
   console.log("Server running in port " + PORT);
});
