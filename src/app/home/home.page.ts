import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  motos = []  

  constructor(private router: Router, private menu: MenuController) {
    const data = this.getJson()
  }

  async getJson() {
    let res = await fetch('http://victor-masip-7e4.alwaysdata.net/api/motos')
    this.motos = await res.json()
  }

  async clickMarca(marca) {
    let res = await fetch('http://victor-masip-7e4.alwaysdata.net/api/motos/' + marca)
    this.motos = await res.json()
  }

  async edit(id, brand, model, image) {
    let navigationExtras: NavigationExtras = {
      state: {
        id: id,
        brand: brand, 
        model: model, 
        image: image
      },
    }
    this.router.navigate(['detail'], navigationExtras);
  }

  goInsert() {
    this.router.navigate(['insert']);
  }

  ionViewWillEnter(){
    this.getJson()
  }

  openMenu() {
    this.menu.open();
  }

}
