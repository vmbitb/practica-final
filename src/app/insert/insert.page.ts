import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-insert',
  templateUrl: './insert.page.html',
  styleUrls: ['./insert.page.scss'],
})
export class InsertPage implements OnInit {

  marca: string = ""
  model: string = ""
  any: string = ""
  preu: string = ""
  image: any = ""

  async postMoto(formData){
    //quitar "/foto" de la url para mi api

    const info=await fetch('http://victor-masip-7e4.alwaysdata.net/api/api/moto',{
      method: "POST",
      body:formData
    })
   console.log(await info.json())
  }


  async insertMoto() {
    const form = new FormData();
    form.append("marca", this.marca);
    form.append("modelo", this.model);
    form.append("year", this.any);
    form.append("precio", this.preu);
    form.append("foto", this.image);
    
    this.postMoto(JSON.stringify(form))

  }
  ngOnInit() {
  }
}
