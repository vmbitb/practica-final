import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  id: any
  brand: any
  model: any
  image: any

  constructor(private route: ActivatedRoute, private router: Router, public alertController: AlertController) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.id = this.router.getCurrentNavigation().extras.state.id;
        this.brand = this.router.getCurrentNavigation().extras.state.brand;
        this.model = this.router.getCurrentNavigation().extras.state.model;
        this.image = this.router.getCurrentNavigation().extras.state.image;
      }
    })
  }  

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Confirmació',
      message: 'Estàs <strong>segur</strong>?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Cancel·lat');
          }
        }, {
          text: 'Sí',
          handler: () => {
            this.deleteMoto(this.id);
          }
        }
      ]
    });

    await alert.present();
  }

  deleteMoto(idMoto: any) {
    const url = "http://victor-masip-7e4.alwaysdata.net/api/motos/id/" + idMoto;
    fetch(url, {
      "method": "DELETE",
    })
    .then(_response => {
        this.router.navigateByUrl('/');
      });
  }



  ngOnInit() {
    console.log(this.id)
  }

}
